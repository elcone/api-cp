from django.apps import AppConfig


class CodigosPostalesConfig(AppConfig):
    name = 'codigos_postales'
