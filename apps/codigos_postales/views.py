from rest_framework import viewsets
from rest_framework import mixins

from .serializers import CodigoPostalSerializer
from .models import CodigoPostal


class CodigoPostalView(mixins.RetrieveModelMixin,
                       viewsets.GenericViewSet):
    serializer_class = CodigoPostalSerializer
    queryset = CodigoPostal.objects.all()
    lookup_field = 'codigo'
    lookup_url_kwargs = 'codigo'
