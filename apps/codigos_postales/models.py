from django.db import models


class CodigoPostal(models.Model):
    codigo = models.CharField(max_length=5)
    ciudad = models.CharField(max_length=50)
    municipio = models.CharField(max_length=50)
    estado = models.CharField(max_length=50)

    def __str__(self):
        return '{codigo} - {ciudad}, {municipio}, {estado}'.format(
            codigo=self.codigo,
            ciudad=self.ciudad,
            municipio=self.municipio,
            estado=self.estado)

    class Meta:
        ordering = ['codigo']
        db_table = 'codigos_postales'


class Asentamiento(models.Model):
    codigo_postal = models.ForeignKey(CodigoPostal, related_name='asentamientos')
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return '{nombre}, {ciudad}, {estado}'.format(
            nombre=self.nombre,
            ciudad=self.codigo_postal.ciudad,
            estado=self.codigo_postal.estado)

    class Meta:
        ordering = ['nombre']
        db_table = 'asentamientos'
