import os
import csv
import zipfile

from django.core.management.base import BaseCommand
import requests

from apps.codigos_postales.models import CodigoPostal, Asentamiento


class Command(BaseCommand):
    ARCHIVO_ZIP = 'codigos.zip'
    ARCHIVO_TXT = 'CPdescarga.txt'

    def descargar_codigos(self):
        url = 'http://www.correosdemexico.gob.mx/lservicios/servicios/CodigoPostal_Exportar.aspx'
        parametros = {
            '__EVENTTARGET': '',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            '__VIEWSTATE': '/wEPDwUKMTIxMDU0NDIwMA9kFgICAQ9kFgICAQ9kFgYCAw8PFgIeBFRleHQFOcOabHRpbWEgQWN0dWFsaXphY2nDs24gZGUgSW5mb3JtYWNpw7NuOiBBZ29zdG8gMjQgZGUgMjAxN2RkAgcPEA8WBh4NRGF0YVRleHRGaWVsZAUDRWRvHg5EYXRhVmFsdWVGaWVsZAUFSWRFZG8eC18hRGF0YUJvdW5kZ2QQFSEjLS0tLS0tLS0tLSBUICBvICBkICBvICBzIC0tLS0tLS0tLS0OQWd1YXNjYWxpZW50ZXMPQmFqYSBDYWxpZm9ybmlhE0JhamEgQ2FsaWZvcm5pYSBTdXIIQ2FtcGVjaGUUQ29haHVpbGEgZGUgWmFyYWdvemEGQ29saW1hB0NoaWFwYXMJQ2hpaHVhaHVhEUNpdWRhZCBkZSBNw6l4aWNvB0R1cmFuZ28KR3VhbmFqdWF0bwhHdWVycmVybwdIaWRhbGdvB0phbGlzY28HTcOpeGljbxRNaWNob2Fjw6FuIGRlIE9jYW1wbwdNb3JlbG9zB05heWFyaXQLTnVldm8gTGXDs24GT2F4YWNhBlB1ZWJsYQpRdWVyw6l0YXJvDFF1aW50YW5hIFJvbxBTYW4gTHVpcyBQb3Rvc8OtB1NpbmFsb2EGU29ub3JhB1RhYmFzY28KVGFtYXVsaXBhcwhUbGF4Y2FsYR9WZXJhY3J1eiBkZSBJZ25hY2lvIGRlIGxhIExsYXZlCFl1Y2F0w6FuCVphY2F0ZWNhcxUhAjAwAjAxAjAyAjAzAjA0AjA1AjA2AjA3AjA4AjA5AjEwAjExAjEyAjEzAjE0AjE1AjE2AjE3AjE4AjE5AjIwAjIxAjIyAjIzAjI0AjI1AjI2AjI3AjI4AjI5AjMwAjMxAjMyFCsDIWdnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2RkAh0PPCsACwBkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBQtidG5EZXNjYXJnYWvbLOsOdDRxiQzg1Qy0yJYnL7mf',
            '__EVENTVALIDATION': '/wEWKALa89H+DwLG/OLvBgLWk4iCCgLWk4SCCgLWk4CCCgLWk7yCCgLWk7iCCgLWk7SCCgLWk7CCCgLWk6yCCgLWk+iBCgLWk+SBCgLJk4iCCgLJk4SCCgLJk4CCCgLJk7yCCgLJk7iCCgLJk7SCCgLJk7CCCgLJk6yCCgLJk+iBCgLJk+SBCgLIk4iCCgLIk4SCCgLIk4CCCgLIk7yCCgLIk7iCCgLIk7SCCgLIk7CCCgLIk6yCCgLIk+iBCgLIk+SBCgLLk4iCCgLLk4SCCgLLk4CCCgLL+uTWBALa4Za4AgK+qOyRAQLI56b6CwL1/KjtBUcCTTOq5UyqkvfqBr64cBjV3L4B',
            'cboEdo': '00',
            'rblTipo': 'txt',
            'btnDescarga.x': '35',
            'btnDescarga.y': '16'
        }

        respuesta = requests.post(url, data=parametros, stream=True)

        with open(self.ARCHIVO_ZIP, 'wb') as archivo:
            for chunk in respuesta.iter_content(chunk_size=1024):
                if chunk:
                    archivo.write(chunk)

    def extraer_zip(self):
        archivo = zipfile.ZipFile(self.ARCHIVO_ZIP, 'r')
        archivo.extractall()
        archivo.close()

    def guardar_codigos(self):
        # la primer línea del archivo csv es un comentario, no debe ser
        # considerada, por lo que se generará un nuevo archivo pero a
        # partir de la segunda línea
        with open(self.ARCHIVO_TXT, 'r', encoding='latin-1') as archivo:
            datos = archivo.readlines()[1:]

        with open(self.ARCHIVO_TXT, 'w', encoding='latin-1') as archivo:
            archivo.writelines(datos)

        # lectura del archivo csv final con la primer línea como encabezados
        with open(self.ARCHIVO_TXT, 'r', encoding='latin-1') as archivo:
            lector = csv.DictReader(archivo, delimiter='|')
            for datos in lector:
                self.guardar_asentamiento(datos)

    def guardar_asentamiento(self, datos):
        codigo_postal, es_nuevo = CodigoPostal.objects.update_or_create(
            codigo=datos.get('d_codigo'),
            defaults={
                'codigo': datos.get('d_codigo'),
                'ciudad': datos.get('d_ciudad'),
                'municipio': datos.get('D_mnpio'),
                'estado': datos.get('d_estado')
            })

        zona = datos.get('d_zona')
        if zona in ['Urbano']:
            asentamiento, es_nuevo = Asentamiento.objects.update_or_create(
                nombre=datos.get('d_asenta'),
                codigo_postal=codigo_postal,
                defaults={
                    'nombre': datos.get('d_asenta'),
                    'codigo_postal': codigo_postal
                })

    def handle(self, *args, **options):
        self.stdout.write('Descargando archivo, esto puede tardar unos minutos')
        self.descargar_codigos()

        self.stdout.write('Extrayendo archivo zip')
        self.extraer_zip()

        self.stdout.write('Actualizando códigos postales, esto puede tardar unos minutos')
        self.guardar_codigos()

        self.stdout.write('Quitando archivos')
        os.remove(self.ARCHIVO_ZIP)
        os.remove(self.ARCHIVO_TXT)

        self.stdout.write('¡Listo!')
