from rest_framework import serializers

from .models import CodigoPostal, Asentamiento


class AsentamientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asentamiento
        fields = [
            'nombre',
        ]


class CodigoPostalSerializer(serializers.ModelSerializer):
    asentamientos = AsentamientoSerializer(many=True)

    class Meta:
        model = CodigoPostal
        fields = [
            'codigo',
            'ciudad',
            'municipio',
            'estado',
            'asentamientos',
        ]
