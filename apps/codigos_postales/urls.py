from django.conf.urls import url, include

from rest_framework import routers

from .views import CodigoPostalView


router = routers.DefaultRouter()
router.register(r'codigospostales', CodigoPostalView, base_name='codigospostales')

urlpatterns = [
    url(r'api/', include(router.urls)),
]
